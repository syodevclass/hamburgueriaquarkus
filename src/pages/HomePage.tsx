import React, { useState } from 'react';
import { Container, Typography, Grid, AppBar, Toolbar, Badge, IconButton } from '@mui/material';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import MenuIcon from '@mui/icons-material/Menu';
import CartDrawer from '../components/criarconta/home/CartDrawer';
import HamburgerCard from '../components/criarconta/home/HamburguerCard';
import hamburger1 from '../assets/hamburguer1.jpg';
import hamburger2 from '../assets/hamburguer2.jpg';
import hamburger3 from '../assets/hamburguer3.jpg';
import hamburger4 from '../assets/hamburguer4.jpg';
import hamburger5 from '../assets/hamburguer5.jpg';
import hamburger6 from '../assets/hamburguer6.jpg';

const Home: React.FC = () => {
  const [cartItems, setCartItems] = useState<{ name: string; price: number }[]>([]);
  const [drawerOpen, setDrawerOpen] = useState(false);

  const handleAddToCart = (item: { name: string; price: number }) => {
    setCartItems((prevItems) => [...prevItems, item]);
  };

  const handleGoToCart = () => {
    setDrawerOpen(true);
  };

  const handleDrawerClose = () => {
    setDrawerOpen(false);
  };

  const handleClearCart = () => {
    setCartItems([]);
  };

  const handleBuy = () => {
    alert('Compra realizada com sucesso!');
    setCartItems([]);
    setDrawerOpen(false);
  };

  const hamburgers = [
    { name: 'Hamburguerão da Fazenda', image: hamburger1, price: 15.99 },
    { name: 'Cheese Mineirão', image: hamburger2, price: 12.99 },
    { name: 'X-Picanha do Sertão', image: hamburger3, price: 18.99 },
    { name: 'X-Pé na Estrada', image: hamburger4, price: 14.99 },
    { name: 'Hamburguerô Caipirado', image: hamburger5, price: 16.99 },
    { name: 'Trem Bom Burger', image: hamburger6, price: 13.99 },
  ];

  return (
    <Container className="container">
      <AppBar position="static" className="appBar">
        <Toolbar className="toolbar">
          <IconButton edge="start" color="inherit" aria-label="menu" onClick={handleDrawerClose} sx={{ mr: 2 }}>
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" className="title">
            Hamburgueria
          </Typography>
          <IconButton color="inherit" onClick={handleGoToCart} className="shoppingCartButton">
            <Badge badgeContent={cartItems.length} color="error">
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
        </Toolbar>
      </AppBar>

      <CartDrawer
        open={drawerOpen}
        onClose={handleDrawerClose}
        cartItems={cartItems}
        onClearCart={handleClearCart}
        onBuy={handleBuy}
      />

      <Grid container spacing={2}>
        {hamburgers.map((hamburger, index) => (
          <Grid item key={index} xs={12} sm={6} md={4}>
            <HamburgerCard
              name={hamburger.name}
              image={hamburger.image}
              price={hamburger.price}
              onAddToCart={() => handleAddToCart({ name: hamburger.name, price: hamburger.price })}
            />
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Home;
