import React, { useState } from 'react';
import LoginForm from '../components/criarconta/login/LoginForm';
import LoginAvatar from '../components/criarconta/login/LoginAvatar';
import Copyright from '../components/criarconta/login/Copyright';
import { useNavigate } from 'react-router-dom';


const SignIn: React.FC = () => {
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const navigate = useNavigate();

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setErrorMessage(null);

    try {
      const data = new FormData(event.currentTarget);
      const response = await fetch('http://localhost:9090/api/usuario/authenticate', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: data.get('email'),
          senha: data.get('password'),
        }),
      });

      if (response.ok) {
        const usuarioDTO = await response.json();
        console.log('Login bem-sucedido!', usuarioDTO);
        navigate('/home')
      } else {
        const result = await response.text();
        setErrorMessage(result || 'Erro de autenticação');
      }
    } catch (error) {
      console.error('Erro ao autenticar:', error);
      setErrorMessage('Erro interno do servidor');
    }
  };

  const containerStyle: React.CSSProperties = {
    height: '100vh',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  };

  return (
    <div style={containerStyle}>
      <LoginAvatar />
      <LoginForm onSubmit={handleSubmit} />
      {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
      <Copyright />
    </div>
  );
};

export default SignIn;
