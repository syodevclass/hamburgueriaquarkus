import React, {  } from 'react';
import CriarContaForm, { UserData } from '../components/criarconta/CriarContaForm';
import { useNavigate } from 'react-router-dom';

const CriarContaPage: React.FC = () => {
  const navigate = useNavigate();

  const handleCriarConta = async (userData: UserData) => {
    try {
      const response = await fetch('http://localhost:9090/api/usuario', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          id: null,
          nome: `${userData.firstName} ${userData.lastName}`,
          email: userData.email,
          senha: userData.password,
        }),
      });

      if (response.ok) {
        console.log('Usuário criado com sucesso!');
        navigate('/');
      } else {
        const errorData = await response.json();
        console.error('Erro ao criar usuário:', errorData);
      }
    } catch (error) {
      console.error('Erro ao criar usuário:', error);
    }
  };

  return (
    <div>
      <CriarContaForm onSubmit={handleCriarConta} />
    </div>
  );
};

export default CriarContaPage;
