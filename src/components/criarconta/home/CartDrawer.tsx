import React from 'react';
import { Drawer, List, ListItem, ListItemText, Button } from '@mui/material';

interface CartDrawerProps {
  open: boolean;
  onClose: () => void;
  cartItems: { name: string; price: number }[];
  onClearCart: () => void;
  onBuy: () => void;
}

const CartDrawer: React.FC<CartDrawerProps> = ({ open, onClose, cartItems, onClearCart, onBuy }) => {
  const deliveryFee = 5.0; // Taxa de entrega

  const getTotalPrice = () => {
    const itemsTotal = cartItems.reduce((total, item) => total + item.price, 0);
    return itemsTotal + deliveryFee;
  };

  return (
    <Drawer anchor="right" open={open} onClose={onClose}>
      <div>
        <List>
          <ListItem>
            <ListItemText primary="Carrinho" />
          </ListItem>
          {cartItems.map((item, index) => (
            <ListItem key={index}>
              <ListItemText primary={`${item.name} - R$${item.price.toFixed(2)}`} />
            </ListItem>
          ))}
          <ListItem>
            <ListItemText primary={`Taxa de Entrega: R$${deliveryFee.toFixed(2)}`} />
          </ListItem>
          <ListItem>
            <ListItemText primary={`Total: R$${getTotalPrice().toFixed(2)}`} />
          </ListItem>
        </List>
        <Button onClick={onClearCart} variant="contained" color="secondary">
          Limpar Carrinho
        </Button>
        <Button onClick={onBuy} variant="contained" color="primary">
          Comprar
        </Button>
      </div>
    </Drawer>
  );
};

export default CartDrawer;
