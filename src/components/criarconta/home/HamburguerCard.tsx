import React from 'react';
import { Card, CardMedia, CardContent, Typography, Button } from '@mui/material';

interface HamburgerCardProps {
  name: string;
  image: string;
  price: number;
  onAddToCart: () => void;
}

const HamburgerCard: React.FC<HamburgerCardProps> = ({ name, image, price, onAddToCart }) => {
  return (
    <Card>
      <CardMedia component="img" height="140" image={image} alt={name} />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          Preço: R${price.toFixed(2)}
        </Typography>
        <Button onClick={onAddToCart} variant="contained" color="primary">
          Adicionar ao Carrinho
        </Button>
      </CardContent>
    </Card>
  );
};

export default HamburgerCard;
