import React from 'react';

interface OrderListProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  orders: any[];
}

const OrderList: React.FC<OrderListProps> = ({ orders }) => {
  return (
    <div>
      <h2>Pedidos Realizados</h2>
      <ul>
        {orders.map((order) => (
          <li key={order.id}>{order.burgerName} - R${order.totalPrice}</li>
        ))}
      </ul>
    </div>
  );
};

export default OrderList;
