

import React from 'react';

interface BurgerListProps {

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  burgers: any[];
}

const BurgerList: React.FC<BurgerListProps> = ({ burgers }) => {
  return (
    <div>
      <h2>Hambúrgueres Disponíveis</h2>
      <ul>
        {burgers.map((burger) => (
          <li key={burger.id}>{burger.name} - R${burger.price}</li>
        ))}
      </ul>
    </div>
  );
};

export default BurgerList;
