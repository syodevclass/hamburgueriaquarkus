import React from 'react';
import styled from 'styled-components';

interface LoginButtonProps {
  onClick: () => void;
}

const Button = styled.button`
  background-color: #4caf50;
  color: white;
  padding: 10px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
`;

const LoginButton: React.FC<LoginButtonProps> = ({ onClick }) => {
  return <Button onClick={onClick}>Entrar</Button>;
};

export default LoginButton;
