package br.com.nathanb.controller;

import br.com.nathanb.dto.UsuarioDTO;
import br.com.nathanb.entity.Usuario;
import br.com.nathanb.service.UsuarioService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import br.com.nathanb.dto.UsuarioDTO;
import br.com.nathanb.entity.Usuario;
import br.com.nathanb.service.UsuarioService;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path( "/api/usuario" )
public class UsuarioController {

    @Inject
    UsuarioService userS;

    @GET
    @Path( "/find" )
    public Response findUsuarioByEmail( Usuario usuario ) throws Exception {
        if ( usuario.getEmail() != null && !usuario.getEmail().isEmpty() && usuario.getSenha() != null
                && !usuario.getSenha().isEmpty() ) {
            String email = usuario.getEmail();
            String senha = usuario.getSenha();

            UsuarioDTO usuarioDTO = userS.findUsuarioByEmailAndSenha( email, senha );

            if ( usuarioDTO != null ) {
                return Response.ok( usuarioDTO ).build();
            } else {
                return Response.status( Response.Status.NOT_FOUND ).entity( "Usuário não encontrado" ).build();
            }
        }

        return Response.status( Response.Status.BAD_REQUEST ).entity( "Email ou senha não podem estar vazios!" )
                .build();
    }

    @POST
    @Transactional
    public void addUsuario( Usuario usuario ) throws Exception {
        userS.addUsuario( usuario );
    }

    @POST
    @Path( "/authenticate" )
    @Consumes( MediaType.APPLICATION_JSON )
    public Response authenticateUsuario( Usuario usuario ) {
        try {
            if ( usuario.getEmail() != null && !usuario.getEmail().isEmpty() && usuario.getSenha() != null
                    && !usuario.getSenha().isEmpty() ) {

                String email = usuario.getEmail();
                String senha = usuario.getSenha();

                UsuarioDTO usuarioDTO = userS.authenticateUsuario( email, senha );

                if ( usuarioDTO != null ) {
                    return Response.ok( usuarioDTO ).build();
                } else {
                    return Response.status( Response.Status.UNAUTHORIZED ).entity( "Credenciais inválidas" ).build();
                }
            } else {
                return Response.status( Response.Status.BAD_REQUEST ).entity( "Email ou senha não podem estar vazios!" )
                        .build();
            }
        } catch ( Exception e ) {
            return Response.status( Response.Status.INTERNAL_SERVER_ERROR ).entity( "Erro interno do servidor" )
                    .build();
        }
    }
}
