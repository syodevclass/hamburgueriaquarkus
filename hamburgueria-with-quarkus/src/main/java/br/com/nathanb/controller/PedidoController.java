package br.com.nathanb.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.nathanb.dto.PedidoDTO;
import br.com.nathanb.entity.Pedido;
import br.com.nathanb.service.PedidoService;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;

@Path("/api/pedidos")
public class PedidoController {
    
    @Inject
    PedidoService pedidoS;

    @GET
    public List< Pedido > retrivePedidos() {

        List< Pedido > pedidos = new ArrayList<>();

        try {

            pedidos = pedidoS.findPedidos();

        } catch ( Exception e ) {
            e.printStackTrace();

        }

        return pedidos;

    }

    @GET
    @Path("/pedidos/pending")
    public List< Pedido > retrivePedidosPending() {

        List< Pedido > pedidos = new ArrayList<>();

        try {

            pedidos = pedidoS.findPedidosPeding();

        } catch ( Exception e ) {
            e.printStackTrace();

        }

        return pedidos;
    }

    @POST
    @Transactional
    public void addPedidos( PedidoDTO pedido ) throws Exception { pedidoS.addPedido( pedido ); }

    
}
