package br.com.nathanb.controller;

import java.util.ArrayList;
import java.util.List;

import br.com.nathanb.entity.Produto;
import br.com.nathanb.service.ProdutoService;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;

@Path("/api/produto")
public class ProdutoController {
    
    @Inject
    ProdutoService produtoS;

    @GET
    public List< Produto > retriveProdutos() {
        
        List< Produto > produtos = new ArrayList<>();
        
        try {
            produtos = produtoS.findAllProdutos();

        } catch ( Exception e ) {
            e.printStackTrace();

        }

        return produtos;

    }

    @POST
    public void addProduto( Produto produto ) throws Exception { produtoS.addProduto( produto ); }

}
