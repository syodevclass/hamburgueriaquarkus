package br.com.nathanb.service;

import java.util.List;

import br.com.nathanb.entity.Item;
import br.com.nathanb.entity.Pedido;
import br.com.nathanb.entity.Produto;
import br.com.nathanb.repository.ItemRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class ItemService {
    
    @Inject
    ItemRepository repoI;

    public void addItem( Pedido pedido, List< Produto > produtos ) throws Exception {

        for ( Produto produto : produtos ) {
            Item item = new Item();
            item.setPedido(pedido);
            item.setProduto(produto);

            repoI.save(item);
            
        }

    }

}
