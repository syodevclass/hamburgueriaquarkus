package br.com.nathanb.service;

import java.util.List;

import br.com.nathanb.entity.Produto;
import br.com.nathanb.repository.ProdutoRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class ProdutoService {
    
    @Inject
    ProdutoRepository repoP;

    public List< Produto > findAllProdutos() {
        return repoP.list();
        
    }

    public void addProduto( Produto produto ) throws Exception {

        if ( produto.getId() == null ) {
            repoP.save(produto);
            System.out.println("Produto registrado!");

        } else {

            Produto exists = repoP.findById( produto.getId() );

            if ( exists == null ) {
				throw new Exception( "Erro ao atualizar o Produto!" );
				
			} else {
				repoP.update(produto);
				System.out.println( "Produto atualizado!" );

			}

        }

    }

}
