package br.com.nathanb.service;

import java.util.List;

import br.com.nathanb.dto.PedidoDTO;
import br.com.nathanb.entity.Item;
import br.com.nathanb.entity.Pedido;
import br.com.nathanb.entity.Produto;
import br.com.nathanb.repository.ItemRepository;
import br.com.nathanb.repository.PedidoRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class PedidoService {
    
    @Inject
    PedidoRepository repoP;

    @Inject
    ItemRepository repoI;

    public List< Pedido > findAllPedidos() {
        return repoP.list();

    }

    public List< Pedido > findPedidos() {
        return repoP.list();


    }

    public List< Pedido > findPedidosPeding() {
        return repoP.listByStatusPeding();

    }

    public void addPedido( PedidoDTO pedido )  throws Exception {

        if ( pedido.getPedido().getId() == null ) {

            Pedido id = repoP.save(pedido.getPedido());
            System.out.println( "Pedido gerado!" + id );

            List< Produto > produtos = pedido.getProdutos();

            for ( Produto produto : produtos ) {
                Item item = new Item();
                item.setPedido(id);
                item.setProduto(produto);

                repoI.save(item);
                    
                }

        } else {

            Pedido exists = repoP.findById( pedido.getPedido().getId() );

            if ( exists != null ) {
                repoP.update(pedido.getPedido());
                 System.out.println( "Pedido atualizado!" );

            } else {
                 throw new Exception( "Erro ao atualizar o pedido!" ); 

            }
        }
    }
    
}
