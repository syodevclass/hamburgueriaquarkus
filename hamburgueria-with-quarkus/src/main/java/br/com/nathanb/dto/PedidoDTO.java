package br.com.nathanb.dto;

import java.util.List;

import br.com.nathanb.entity.Hamburgueria;
import br.com.nathanb.entity.Pedido;
import br.com.nathanb.entity.Produto;
import br.com.nathanb.entity.Usuario;

public class PedidoDTO {
    
    private Pedido pedido;
    private Usuario usuario;
    private Hamburgueria hamburgueria;
    private List< Produto > produtos;
    
    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Hamburgueria getHamburgueria() {
        return hamburgueria;
    }

    public void setHamburgueria(Hamburgueria hamburgueria) {
        this.hamburgueria = hamburgueria;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
    

}
