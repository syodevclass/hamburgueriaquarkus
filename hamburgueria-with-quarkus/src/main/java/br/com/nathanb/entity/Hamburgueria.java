package br.com.nathanb.entity;

import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table( name = "hamburgueria" )
public class Hamburgueria {
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY )
	@Column( name = "id_hamburgueria", unique = true, nullable = false )
	private Integer id;
	
	@Column( name = "nm_hamburgueria", unique = true, nullable = false )
	private String nome;
	
	@OneToMany(fetch = FetchType.LAZY)
	private List< Pedido > pedidos;
	
	@OneToMany(fetch = FetchType.LAZY)
	private List< Item > itens;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public List<Item> getItens() {
        return itens;
    }

    public void setItens(List<Item> itens) {
        this.itens = itens;
    }
    
    @Override
	public String toString() {
		return "Hamburgueria [id=" + id + ", nome=" + nome + "]";
	}
    		
}
