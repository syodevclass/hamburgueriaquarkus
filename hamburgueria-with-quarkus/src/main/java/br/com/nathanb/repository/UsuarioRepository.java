package br.com.nathanb.repository;

import java.util.List;

import br.com.nathanb.entity.Usuario;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class UsuarioRepository implements PanacheRepository< Usuario > {
 
    @Inject
    EntityManager entityManager;

    @Transactional
    public List< Usuario > list() {
        return this.list();

    } 

    @Transactional
    public Usuario findById( Integer id ) {
        return this.findById( id );

    } 

    @Transactional
    public Usuario findByEmail( String email ) {
        
        System.out.println(email);
        
        return entityManager.createQuery( "SELECT u FROM Usuario u WHERE u.email = :email", Usuario.class )
            .setParameter( "email", email )
            .getSingleResult();

    }

    @Transactional
    public void save( Usuario usuario ) {

        entityManager.merge( usuario );
        System.out.println( "Usuario registrado!");

    }

    @Transactional
    public void update( Usuario usuario ) throws Exception {

    entityManager.merge( usuario );
    System.out.println( "Informações do usuario " + usuario.getId() + " foram atualizadas!" );

    }
    
    @Transactional
    public void delete( Integer id ) {
        this.delete(id);
    }


}
