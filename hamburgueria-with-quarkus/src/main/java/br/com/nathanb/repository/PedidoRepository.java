package br.com.nathanb.repository;

import java.time.LocalDateTime;
import java.util.List;

import br.com.nathanb.entity.Pedido;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class PedidoRepository implements PanacheRepository< Pedido > {

    @Inject
    EntityManager entityManager;

    @Transactional
    public List< Pedido > list() {
        return this.listAll();

    }

    @Transactional
    public List< Pedido > listByDate( LocalDateTime data ) {

        System.out.println(data);

        return entityManager.createQuery( "SELECT p FROM Pedido p WHERE p.data = :data", Pedido.class )
        .setParameter("data", data )
        .getResultList();

    }

    @Transactional
    public List< Pedido > listByStatus( String status ) {

        System.out.println(status);

        return entityManager.createQuery( "SELECT p FROM Pedido p WHERE p.status = :status", Pedido.class )
        .setParameter("status", status )
        .getResultList();

    }

    @Transactional
    public List< Pedido > listByStatusPeding() {

        return entityManager.createQuery("SELECT p FROM Pedido p WHERE p.status = 'GERADO' OR p.status = 'PREPARANDO' ", Pedido.class )
        .getResultList();

    }

    @Transactional
    public Pedido findById( Integer id ) {
       return this.findById( id );

    }

    @Transactional
    public Pedido save( Pedido pedido ) {
        this.persist( pedido );
        System.out.println( "Pedido gerado!" );

        return pedido;
    }

    @Transactional
    public void update( Pedido pedido ) {
        entityManager.merge( pedido );
        System.out.println( "Pedido " + pedido.getId() + " atualizado!" );

    }

    @Transactional
    public void delete( Pedido pedido ) {
        this.delete( pedido );
        System.out.println( "Pedido excluido!" );

    }

}