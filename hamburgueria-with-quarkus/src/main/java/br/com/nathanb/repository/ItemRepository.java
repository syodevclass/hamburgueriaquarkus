package br.com.nathanb.repository;

import java.util.List;

import br.com.nathanb.entity.Item;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class ItemRepository implements PanacheRepository< Item > {
    
    @Inject
    EntityManager entityManager;
	
	@Transactional
	public List< Item > list() {
		return this.listAll();
		
	}
	
	@Transactional
	public Item findById( Integer id ) {
		return this.findById( id );
		
	}
	
	@Transactional
	public void save( Item item ) {
				
			this.persist( item );
			System.out.println( "Item registrado!" );
					
	}

	@Transactional
	public void delete( Integer id ) {	
		this.delete(id);
		
	}

}
