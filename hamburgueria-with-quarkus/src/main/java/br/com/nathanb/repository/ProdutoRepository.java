package br.com.nathanb.repository;

import java.util.List;

import br.com.nathanb.entity.Produto;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class ProdutoRepository implements PanacheRepository< Produto > {
    
    @Inject
    EntityManager entityManager;

    @Transactional
    public List< Produto > list() {
        return this.listAll();

    }

    @Transactional
    public Produto findById( Integer id ) {
        return this.findById( id );

    }

    @Transactional
    public void save( Produto produto ) {
        
        this.persist( produto );
        System.out.println("Produto registrado!");

    }

    @Transactional
    public void update( Produto produto ) {

        entityManager.merge( produto );
        System.out.println("Produto " + produto.getId() + " atualizado!");

    }

    @Transactional
    public void delete( Produto produto ) {
        this.delete(produto);

    }

}
