package br.com.nathanb.repository;

import java.util.List;

import br.com.nathanb.entity.Hamburgueria;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class HamburgueriaRepository implements PanacheRepository< Hamburgueria > {
	
	@Inject
    EntityManager entityManager;
	
	@Transactional
	public List< Hamburgueria > list() {
		return this.listAll();
		
	}
	
	@Transactional
	public Hamburgueria findById( Integer id ) {
		return this.findById( id );
		
	}
	
	@Transactional
	public void save( Hamburgueria hamburgueria ) {
				
			this.persist( hamburgueria );
			System.out.println( "Hamburgueria registrada!" );
					
	}
	
	@Transactional
	public void update( Hamburgueria hamburgueria ) {
		
		entityManager.merge( hamburgueria );
		System.out.println( "Informações da hamburgueria " + hamburgueria.getId() + " atualizada!" );

	}

	@Transactional
	public void delete( Integer id ) {	
		this.delete(id);
		
	}
	
}
